window.fakeStorage = {
  _data: {},

  setItem: function (id, val) {
    return this._data[id] = String(val);
  },

  getItem: function (id) {
    return this._data.hasOwnProperty(id) ? this._data[id] : undefined;
  },

  removeItem: function (id) {
    return true//delete this._data[id];
  },

  clear: function () {
    return true//this._data = {};
  }
};

function LocalStorageManager() {
  this.bestScoreKey     = "bestScore";
  this.gameStateKey     = "gameState";

  var supported = this.localStorageSupported();
  this.storage = supported ? window.localStorage : window.fakeStorage;
}

LocalStorageManager.prototype.localStorageSupported = function () {
  var testKey = "test";

  try {
    var storage = window.localStorage;
    storage.setItem(testKey, "1");
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
};

// Best score getters/setters
LocalStorageManager.prototype.getBestScore = function () {
  return this.storage.getItem(this.bestScoreKey) || 0;
};

LocalStorageManager.prototype.setBestScore = function (score) {
  this.storage.setItem(this.bestScoreKey, score);
};

// Game state getters/setters and clearing
LocalStorageManager.prototype.getGameState = function () {
  var stateJSON = this.storage.getItem(this.gameStateKey);
  return stateJSON ? JSON.parse(stateJSON) : null;
};

LocalStorageManager.prototype.setGameState = function (gameState) {
  console.log('set localStorageSupported')
  console.log(gameState)
  if(gameState.score!=0){
  localStorage.setItem('last_update_score',JSON.stringify(gameState))
  localStorage.setItem('last_score',gameState.score) 
}
  
  this.storage.setItem(this.gameStateKey, JSON.stringify(gameState));
  

};

LocalStorageManager.prototype.clearGameState = function () {
   localStorage.setItem('last_update_score',localStorage.getItem("gameState"));
   console.log('before clear data ')
   console.log(JSON.parse(localStorage.getItem("gameState")));
   localStorage.setItem('last_score',JSON.parse(localStorage.getItem("gameState")).score) 
  this.storage.removeItem(this.gameStateKey);
};
