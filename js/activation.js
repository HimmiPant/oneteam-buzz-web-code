
    $(window).on('load', function () {
      $('#preloader').fadeOut('slow');
    });
  

    $(document).ready(function () {
      if (localStorage.getItem('play_store') == 1) {
        $(".play-store").css('visibility', 'visible');
      }

      if (localStorage.getItem('server') == 'testing') {
        $(".scanner").css('display', 'inline-block');
      }

      if (localStorage.getItem("user_id")) {
        function call_dashboard() {
          var id = JSON.parse(localStorage.getItem("user_id"));

          var device_type = localStorage.getItem('device_type');
          if (device_type == 'browser') {
            var data_type = { "user_id": id };
          } else {
            if (device_type == 'iOS') {
              device_type = 'Iphone';
            } else if (device_type == 'Android') {
              device_type = 'Android';
            }


            var data_type = {
              "user_id": id,
              'device_type': device_type,
              'device_token': localStorage.getItem('device_token')
            };
          }
          $.ajax({

            url: localStorage.getItem("base_url") + 'dashboard',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data_type),
            type: 'POST',
            dataType: 'json',
            async: true,
            cache: true,
            success: function (output) {
              if (output.status == 1000) {

                localStorage.setItem("user_data", JSON.stringify(output.data));

                localStorage.removeItem("mobile_no");
                localStorage.removeItem("activation_code");

              } else {
              }
            },
            error: function () {
              alert("Error in loading records. Please refresh the page and try again.");
            }
          });
        }
        call_dashboard();
        function run_file() {
          var user_data = JSON.parse(localStorage.getItem("user_data"));
          var footer_menu = user_data.app_info.bottom_navigation;
          for (var j = 0; j < footer_menu.length; j++) {


            if (footer_menu[j].menu_id === '1' && footer_menu[j].status === 1) {
              window.location.href = 'feed.html';
              break;

            }

            if ((footer_menu[j].status === 1) && (footer_menu[j].menu_id === '2')) {
              window.location.href = 'message.html';
              break;
            }


            if ((footer_menu[j].status === 1) && (footer_menu[j].menu_id === '3')) {
              window.location.href = 'itinerary.html';
              break;

            }

            if ((footer_menu[j].status === 1) && (footer_menu[j].menu_id === '4')) {
              window.location.href = 'survey.html';
              break;
            }
          }
        }
        if (localStorage.getItem("platform") == 'android') {
          run_file();
        } else {

          run_file();
        }
      } else {
        $('.container-fluid').fadeIn('fast');
        $('#preloader').fadeOut('slow');
      }

      $('#first').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          $('#verify').click()
        }
      });
      $('#second').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          $('#verify').click()
        }
      });
      $('#third').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          $('#verify').click()
        }
      });
      $('#fourth').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          $('#verify').click()
        }
      });
      $("#verify").click(function () {

        var first = document.getElementById("first").value;
        var second = document.getElementById("second").value;
        var third = document.getElementById("third").value;
        var fourth = document.getElementById("fourth").value;
        if (first == '' || second == '' || third == '' || fourth == '') { alert("Please Enter Activation Code."); }
        else {

          var data_type = { "activation_code": first + second + third + fourth };
          $('#preloader').fadeIn('fast');
          $.ajax({
            async: true,
            crossDomain: true,
            url: localStorage.getItem("base_url") + 'activationcode',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data_type),
            type: 'POST',
            dataType: 'json',
            cache: true,
            success: function (output) {
              if (output.status == 1000) {
                localStorage.setItem("activation", JSON.stringify(output.data));
                localStorage.setItem("activation_code", JSON.stringify(output.data.company.activation_code));

                window.location.href = 'login.html';

              } else {
                alert("Activation Not Found. Please check Activation Code.!");
                $('#preloader').fadeOut('slow');
              }
            },
            error: function () {
              alert("Error in loading records. Please refresh the page and try again.");
              $('#preloader').fadeOut('slow');
            }
          });
        }

      });
    });
  
    var container = document.getElementsByClassName("activation-code")[0];
    container.onkeyup = function (e) {
      var target = e.srcElement || e.target;
      var maxLength = parseInt(target.attributes["maxlength"].value, 2);

      var myLength = target.value.length;
      if (myLength >= maxLength) {
        var next = target;

        while (next = next.nextElementSibling) {
          if (next == null)
            break;
          if (next.tagName.toLowerCase() === "input") {
            next.focus();
            break;
          }
        }
      }
      else if (myLength === 0) {
        var previous = target;
        while (previous = previous.previousElementSibling) {
          if (previous == null)
            break;
          if (previous.tagName.toLowerCase() === "input") {
            previous.focus();
            break;
          }
        }
      }
    }