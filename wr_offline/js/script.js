
  (
      ()=>{
        var swiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
          
            // If we need pagination
            pagination: {
              el: '.swiper-pagination',
            },
          
            // Navigation arrows
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
            autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                  },
          
            // And if we need scrollbar
            
          });
      
             fetch(`${localStorage.getItem('base_url')}offline_content_categories`,{
                 method: "POST",
                 body: JSON.stringify({
                   user_id :  localStorage.getItem("user_id"),
                   company_id :localStorage.getItem("company_id")
                })
             })
             .then(data => data.json())
             .then(response => {
                 document.querySelector('.loader').style.cssText= 'display:none';
                 if(response.status == 1000){
                
                 response.data.forEach(item => {
                  //    document.querySelector('.events').innerHTML += `
                  //    <div class="grid_item" category="${item.category_title}" company="${item.company}" url="${item.url}" id="${item.id}" type="${item.type}" onclick="loadSessions(this)">
                  //    <div class="event_box">
                  //    <div class="img_box"><img src="${item.image}" alt=""></div>
                  //    <div class="overlay"></div>
                  //    <div class="content">${item.category_title}</div>
                  //    </div>
                  //    </div>
                  //    `;
                  console.log("..adding..",item.category_title);
                  swiper.prependSlide(
                 
                  );
                 });
                 }
                 if(response.status == 1001){
                     console.log(response.message);
                 }
             }).catch((error)=>{
                 console.log(error);
             })
         })();


function loadSessions(el){
              let company = el.getAttribute('company');
              let id = el.getAttribute('id');
              let type = el.getAttribute('type');
              let url = el.getAttribute('url');
              let category = el.getAttribute('category');
              if(type == 2){
                  window.location.href = 'link.html?link='+url;
              }
              else{
                      localStorage.setItem("company_id",company);
                      localStorage.setItem("category_id",id);
                      localStorage.setItem("type",type);
                      localStorage.setItem("category",category);
                      window.location.href = 'list.html'; 
                   }
              }
       