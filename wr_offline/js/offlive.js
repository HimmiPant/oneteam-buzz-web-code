var platform = {
  Android: function() {
      return navigator.userAgent.match(/Android/i);
  },
  IOS: function() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Windows: function() {
      return navigator.userAgent.match(/Windows/i);
  }
};
let interval = null;
(()=>{
  let data = JSON.parse(localStorage.getItem('session_data'));
  
  localStorage.setItem("live_stream_url_id",data.id);     
  localStorage.setItem("video_url",data.streaming_url);      

})

();


function post_time(milidate) {
  if(isNaN(milidate)){
      return "";
  }else{
  var date = new Date(milidate);
  var monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];
  var day = date.getDate();
  var monthIndex = date.getMonth();
  var d = date.getHours();
  var m = date.getMinutes();
  var y = date.getYear();
  y = y + 1900;
  if (m < 10) { var new_m = '0' + m } else { var new_m = m; }
  if (d >= 12) {
    if (d > 12) { var new_d = d - 12; } else { new_d = d; }
    if (new_d < 10) { new_d = '0' + new_d; } else { new_d = new_d; }
    var s = "p.m.";
  } else {
    if (d < 10) { var new_d = '0' + d; } else { var new_d = d; }
    var s = "a.m.";
  }

  return day + ' ' + monthNames[monthIndex] + ' ' + y + ' ' + new_d + ':' + new_m + ' ' + s;
}
  }


function showAlert(msg){
$.alert({
title: 'Alert!',
content: msg
});
}














